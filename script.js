'use strict';
let clicked = false; //global variable

function clickEvent() {
  if (!clicked) {
    clicked = true;
    console.log('clicked!!');
    setTimeout(function () {
      clicked = false;
    }, 3000);
  }
}

function setMyHandler() {
  let cards = document.querySelectorAll('.faq-card');
  console.log(cards);
  for (let i = 0; i < cards.length; i++) {
    cards[i].onclick = function () {
      document
        .querySelector(`.fa-angle-down-${i + 1}`)
        .classList.toggle('hidden');
      document.querySelector(`.fa-times-${i + 1}`).classList.toggle('hidden');
      console.log('clickeddddd');
    };
  }
}

setMyHandler();

const toggleSwitch = document.querySelector(
  '.theme-switch input[type="checkbox"]'
);
const currentTheme = localStorage.getItem('theme');

if (currentTheme) {
  document.documentElement.setAttribute('data-theme', currentTheme);

  if (currentTheme === 'dark') {
    toggleSwitch.checked = true;
  }
}

function switchTheme(e) {
  if (e.target.checked) {
    document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('theme', 'dark');
  } else {
    document.documentElement.setAttribute('data-theme', 'light');
    localStorage.setItem('theme', 'light');
  }
}

toggleSwitch.addEventListener('change', switchTheme, false);
// function Display() {
//   if (!clicked) {
//     document.querySelector('.fa-angle-down').classList.toggle('hidden');
//     document.querySelector('.fa-times').classList.toggle('hidden');
//     console.log('clickeddddd');
//   }
// }
